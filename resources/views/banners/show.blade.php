@extends('layout')

@section('content')

    <div class="row">
        <div class="col-md-4">
            <h1>{{ $banner->street }} ({{ $banner->city }})</h1>
            <h3>{{ $banner->price }}</h3>

            <div class="description">{!! $banner->description !!}</div>
        </div>

        <div class="col-md-8">
            @foreach($banner->photos->chunk(4) as $set)
                <div class="row">
                    @foreach ($set as $photo)
                        <div class="col_md_3">
                            <img src="/{{ $photo->thumbnail_path }}" alt="">
                        </div>
                    @endforeach
                </div>
            @endforeach

                @if (auth()->check())
                    <form id="addmyPhotos" action="/{{ $banner->zip }}/{{ $banner->street }}/photos" class="dropzone" method="POST">
                        @csrf

                    </form>
                @endif

        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.js"
            integrity="sha512-4p9OjnfBk18Aavg91853yEZCA7ywJYcZpFt+YB+p+gLNPFIAlt2zMBGzTxREYh+sHFsttK0CTYephWaY7I3Wbw=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        Dropzone.options.addmyPhotos = {
            paramName: 'photo',
            maxFiles: 3,
            acceptedFiles: '.jpg , .png , .jpeg',
        }
    </script>
@stop
