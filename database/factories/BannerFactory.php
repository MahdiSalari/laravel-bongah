<?php

namespace Database\Factories;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Banner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'       =>\App\Models\User::factory()->create()->id,
            'street'        =>$this->faker->streetAddress,
            'city'          =>$this->faker->city,
            'zip'           =>$this->faker->postcode,
            'country'       =>$this->faker->country,
            'state'         =>$this->faker->state,
            'price'         =>$this->faker->numberBetween(10000,50000),
            'description'   =>$this->faker->paragraphs(3 , true),
        ];
    }
}
